import React, { Component } from 'react';
import { ReactComponent as XSvg } from '../../Images/xxl-x.svg';
import { ReactComponent as OSvg } from '../../Images/xxl-zero.svg';
import './Cell.css';

class Cell extends Component {
    render() {
        let label = null;
        if (this.props.value != null) {
            if (this.props.value) {
                label = <OSvg />;
            } else {
                label = <XSvg />;
            }
        }

        return (
            <div className="cell">
                {label}
            </div>
        );
    }
}

export default Cell;