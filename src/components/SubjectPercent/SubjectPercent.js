import React, { Component } from 'react';
import './SubjectPercent.css';

class SubjectPercent extends Component {
    render() {
        return <div className="subject-percent">{this.props.winPercentage}% побед</div>;
    }
}

export default SubjectPercent;