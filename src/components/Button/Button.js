import React, { Component } from 'react';
import './Button.css';

class Button extends Component {
    render() {
        return (
            <button className="primary"><label>{this.props.text}</label></button>
        );
    }
}

export default Button;