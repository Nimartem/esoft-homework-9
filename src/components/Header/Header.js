import React, { Component } from 'react';
import Link from '../Link/Link';
import './Header.css';
import { ReactComponent as SLogo } from '../../Images/s-logo.svg';
import { ReactComponent as SignoutIcon } from '../../Images/signout-icon.svg';

class Header extends Component {
    render() {
        return (
        <header>
            <div class="logo"><SLogo /></div>
            <div class="nav-panel">
                <Link name="Игровое поле" active={this.props.links[0]} />
                <Link name="Рейтинг" active={this.props.links[1]} />
                <Link name="Активные игроки" active={this.props.links[2]} />
                <Link name="История игр" active={this.props.links[3]} />
                <Link name="Список игроков" active={this.props.links[4]} />
            </div>
            <button className="signout"><SignoutIcon /></button>
        </header>);
    }
}

export default Header;