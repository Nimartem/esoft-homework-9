import React, { Component } from 'react';
import '../rating.css'

class PlayerStats extends Component {
    render() {
        return (
            <tr>
                <td>{this.props.fullName}</td>
                <td>{this.props.totalGames}</td>
                <td>{this.props.wins}</td>
                <td>{this.props.loses}</td>
                <td>{this.props.winningPercentage}%</td>
            </tr>
        );
    }
}

export default PlayerStats;