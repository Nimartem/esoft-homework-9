import React, { Component } from 'react';
import { ReactComponent as X } from '../../Images/x.svg';
import { ReactComponent as Zero } from '../../Images/zero.svg';
import SubjectName from '../SubjectName/SubjectName';
import SubjectPercent from '../SubjectPercent/SubjectPercent';
import H1 from '../H1/H1';

import './SubjectList.css';

class SubjectList extends Component {
    render() {
        return (
            <div className="subject-list">
                <H1 text="Игроки" />
                <div className="container">
                    <div className="subject-container">
                        <div><Zero /></div>
                        <div className="subject-info">
                            <SubjectName subjectName="Пупкин Владелен Игоревич" />
                            <SubjectPercent winPercentage={63} />
                        </div>
                    </div>
                    <div className="subject-container">
                        <div><X /></div>
                        <div className="subject-info">
                        <SubjectName subjectName="Плюшкина Екатерина Викторовна" />
                            <SubjectPercent winPercentage={23} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SubjectList;