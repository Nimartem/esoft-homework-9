import React, { Component } from 'react';
import './TableContainer.css';
import TableRows from '../../components/TableRows/TableRows';
import H1 from '../../components/H1/H1';

class TableContainer extends Component {
    render() {
        let ths = [];
        for (const th of this.props.ths) {
            ths.push(<th>{th}</th>);
        }

        return (
            <div class = "table-container">
                <H1 text={this.props.tableName} />
                <table>
                <tr>
                    {ths}
                </tr>
                <TableRows tds={this.props.tds}/>
                </table>
            </div>
        );
    }
}

export default TableContainer;

