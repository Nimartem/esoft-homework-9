import React, { Component } from 'react';
import './Link.css';

class Link extends Component {
    render() {
        if (this.props.active) {
            return <div className="active">{this.props.name}</div>;
        }
        return <div>{this.props.name}</div>  
    }
}

export default Link;