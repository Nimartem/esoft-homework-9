import React, { Component } from 'react';
import './Message.css';

class Message extends Component {
    render() {
        let me = "msg-container ";
        if (this.props.me) {
            me += "me";
        } else {
            me += "other";
        }

        let fullname = "fullname ";
        if (this.props.mode) {
            fullname += "zero";
        } else {
            fullname += 'x';
        }

        return (
            <div className={me}>
                <div className="msg-header">
                    <div className={fullname}>{this.props.subjectName}</div>
                    <div className="time">{this.props.time}</div>
                </div>
                <div className="msg-body">{this.props.msgBody}</div>
            </div>
        );
    }
}

export default Message;