import React, { Component } from 'react';
import './SubjectName.css';

class SubjectName extends Component {
    render() {
        return <div className="subject-name">{this.props.subjectName}</div>;
    }
}

export default SubjectName;