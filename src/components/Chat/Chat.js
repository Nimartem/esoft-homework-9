import React, { Component } from 'react';
import './Chat.css';

import Message from '../../components/Message/Message';
import TextField from '../../components/TextField/TextField';

import { ReactComponent as SendBtn } from '../../Images/send-btn.svg';

class Chat extends Component {
    render() {
        return (
            <div class="chat-container">
                <div class="msgs-container">
                    <Message me={true} time="13:40" subjectName="Плюшкина Екатерина Пордовна" msgBody="LULW" mode={false}/>
                    <Message me={false} time="13:42" subjectName="АФРВА" msgBody="fkadgja" mode={true} />
                </div>
                <div class="msg-interactive-elements">
                    <TextField placeholder="Сообщение..." />
                    <button><SendBtn /></button>
                </div>
            </div>
        );
    }
}

export default Chat;