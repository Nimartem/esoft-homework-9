import React, { Component } from 'react';

import XSvg from '../../Images/x.svg';
import OSvg from '../../Images/zero.svg';
import trophy from '../../Images/trophy.svg'

import './GameHistoryPlayer.css';

class GameHistoryPlayer extends Component {
    render() {
        var value = XSvg;
        if (this.props.mode) {
            value = OSvg;
        }

        return (
            <span className="GameHistoryPlayer">
                <img 
                    src={value}
                    style={{ height: '16px', width: '16px' }}
                /> <span>Терещенко У.Р.</span>
                <img src={trophy}/>
            </span>
        );
    }
}

export default GameHistoryPlayer;