import React, { Component } from 'react';
import Cell from '../Cell/Cell';
import './GameField.css';

class GameField extends Component {
    render() {
        return (
            <div className="game-board">
                <Cell />
                <Cell />
                <Cell value={false} />
                <Cell value={true} />
                <Cell value={true} />
                <Cell />
                <Cell />
                <Cell value={false} />
                <Cell />
            </div>
        );
    }
}

export default GameField;