import React, { Component } from 'react';
import './TableRows.css';

class TableRows extends Component {
    render() {
        let tds = [];
        for (const td of this.props.tds) {
            tds.push(<td>{td}</td>);
        }

        return (
            <tr>
                {tds}
            </tr>
        );
    }
}

export default TableRows;