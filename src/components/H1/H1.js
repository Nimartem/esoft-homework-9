import React, { Component } from 'react';
import './H1.css';

class H1 extends Component {
    render() {
        return (
            <h1 class="h1-reusable">
                { this.props.text }
            </h1>
        );
    }
}

export default H1;