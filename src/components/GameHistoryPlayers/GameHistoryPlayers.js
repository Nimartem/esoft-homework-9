import React, { Component } from 'react';
import GameHistoryPlayer from '../GameHistoryPlayer/GameHistoryPlayer';

import './GameHistoryPlayers.css';

class GameHistoryPlayers extends Component {
    render() {
        return (
            <div className="GameHistoryPlayers">
                <GameHistoryPlayer mode={true} />
                <span class="vs">против</span>
                <GameHistoryPlayer mode={false} />
            </div>
        );
    }
}

export default GameHistoryPlayers;