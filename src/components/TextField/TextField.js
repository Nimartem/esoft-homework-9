import React, { Component } from 'react';
import './TextField.css';

class TextField extends Component {
    render() {
        return (
            <div className="textfield">
                <input type="text" placeholder={this.props.placeholder} className="text" name={this.props.name}></input>
            </div>
        );
    }
}

export default TextField;