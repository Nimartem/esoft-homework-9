import Header from '../components/Header/Header';
import SubjectList from '../components/SubjectList/SubjectList';
import GameField from '../components/GameField/GameField';
import Chat from '../components/Chat/Chat';

import { ReactComponent as XSvg } from '../Images/x.svg';
import { ReactComponent as OSvg } from '../Images/zero.svg';

import '../stylesheets/xo-panel.css';

function XOPanel() {
  return (
    <div class="react-page">
    <Header links={[true, false, false, false, false]} />
    <div class="main-container">
        <SubjectList />
        <div class="game-container">
            <div class="game-time">05:12</div>
            <GameField />
            <div class="game-step">Ходит&nbsp;<XSvg />&nbsp;Плюшкина Екатерина</div>
        </div>
        <Chat />
    </div>
    </div>
  );
}

export default XOPanel;