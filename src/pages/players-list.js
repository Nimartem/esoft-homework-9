import '../stylesheets/players-list.css'; 

import Header from '../components/Header/Header';
import TableContainer from '../components/TableContainer/TableContainer';
import Button from '../components/Button/Button';

import { ReactComponent as Female } from '../Images/female.svg';
import { ReactComponent as Blocked } from '../Images/blocked.svg';
import { ReactComponent as Unblock } from '../Images/unblock.svg';

function PlayersList() {
  return (
    <div class="react-page">
      <Header links={[false, false, false, false, true]} />
      <main>
        <TableContainer ths={['ФИО', 'Возраст', 'Пол', 'Статус', 'Создан', 'Изменен', <Button text="Добавить игрока" />]}
         tds={[1, 2, <Female />, <Blocked />, 5, 6, <Unblock />]} />
      </main>
    </div>
  );
}

export default PlayersList;