import '../stylesheets/game-history.css'; 

import Header from '../components/Header/Header';
import TableContainer from '../components/TableContainer/TableContainer';

import GameHistoryPlayers from '../components/GameHistoryPlayers/GameHistoryPlayers';

function GameHistory() {
  return (
    <div class="react-page">
      <Header links={[false, false, false, true, false]} />
      <main>
        <TableContainer tableName="История игр" ths={['Игроки', 'Дата', 'Время игры']} tds={[<GameHistoryPlayers />, 2, 3]} />
      </main>
    </div>
  );
}

export default GameHistory;