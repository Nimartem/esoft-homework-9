import TextField from '../components/TextField/TextField';
import H1 from '../components/H1/H1'; 
import Button from '../components/Button/Button';

import { ReactComponent as Dog } from '../Images/casual-life-3d-blue-scared-ghost 1.svg';

import '../stylesheets/auth.css'; 

function Auth() {
  return (
    <div class="react-page">
      <div class="modal">
        <div>
            <Dog />
        </div>
        <H1 text='Войдите в игру' />
        <TextField placeholder="Логин" />
        <TextField placeholder="Пароль" />
        <Button text="Войти" />
       </div>
    </div>
  );
}

export default Auth;