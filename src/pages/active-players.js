import '../stylesheets/rating.css'; 

import Header from '../components/Header/Header';
import TableContainer from '../components/TableContainer/TableContainer';

function ActivePlayers() {
  return (
    <div class="react-page">
      <Header links={[false, false, true, false, false]} />
      <main>
        <TableContainer ths={['ФИО', 'Всего игр', 'Победы', 'Проигрыши', 'Процент побед']} />
      </main>
    </div>
  );
}

export default ActivePlayers;