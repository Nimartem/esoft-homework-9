import '../stylesheets/rating.css'; 

import Header from '../components/Header/Header';
import TableContainer from '../components/TableContainer/TableContainer';

function Rating() {
  return (
    <div class="react-page">
      <Header links={[false, true, false, false, false]} />
      <main>
        <TableContainer tableName="Рейтинг игроков" ths={['ФИО', 'Всего игр', 'Победы', 'Проигрыши', 'Процент побед']} tds={[1, 2, 3, 4, 5]} />
      </main>
    </div>
  );
}

export default Rating;