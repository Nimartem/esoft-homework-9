import Page from './pages/players-list';

function App() {
  return (
    <div>
      <Page />
    </div>
  );
}

export default App;
